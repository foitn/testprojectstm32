# Room for variables
$ProjectName = "TestProjectSTM32"
$current_location = $PSScriptRoot
$cube_ide_executable = "C:\ST\STM32CubeIDE_1.6.1\STM32CubeIDE\stm32cubeidec.exe"

echo "PSScriptRoot: $PSScriptRoot"
echo "Current location: $current_location"
echo "Moving to workspace folder"
pushd $current_location\..\..
$location = Get-Location
echo "Current location: $location"

$command = "&'$cube_ide_executable' --launcher.suppressErrors -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -import '$ProjectName' -build $ProjectName"

echo "Executing: $command"
iex $command

popd
